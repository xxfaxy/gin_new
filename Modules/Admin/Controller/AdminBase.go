package AdminController

import (
    "Dry/Function"
    "Dry/Modules/Base/Controller"
    "github.com/gin-gonic/gin"
    "net/http"
)

type AdminBase struct {
    BaseController.Base
    Data gin.H
    File string
}

func (that *AdminBase) Init() {
    that.Data = gin.H{}
}

func (that *AdminBase) addFunction() {
    that.Data["GetDateTime"] = Function.GetDateTime()
}

func (that *AdminBase) SetData(name string, data interface{}) {
    that.Data[name] = data
}

func (that *AdminBase) SetFile(name string) {
    that.File = name
}

func (that *AdminBase) JSON(c *gin.Context) {
    c.JSON(http.StatusOK, that.Data)
}

func (that *AdminBase) HTML(c *gin.Context) {
    that.addFunction()
    c.HTML(http.StatusOK, that.File, that.Data)
}
