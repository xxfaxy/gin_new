package AdminController

import (
    "github.com/gin-gonic/gin"
    "net/http"
)

type Demo struct {
    AdminBase
}

func GetDemo() *Demo {
    that := new(Demo)
    return that
}

func (*Demo) Test01WithGet(c *gin.Context) {
    c.JSON(http.StatusOK, gin.H{"name": "Test01"})
}

func (*Demo) Test02WithPost(c *gin.Context) {
    c.JSON(http.StatusOK, gin.H{"name": "Test02"})
}
