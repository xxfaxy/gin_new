package ApiController

import (
    "Dry/Database"
    "Dry/Model"
    "github.com/gin-gonic/gin"
    "net/http"
)

type Demo struct {
    ApiBase
}

func GetDemo() *Demo {
    that := new(Demo)
    return that
}

func (*Demo) Test01WithGet(c *gin.Context) {
    c.JSON(http.StatusOK, gin.H{"name": "Test01"})
}

func (*Demo) Test02WithPost(c *gin.Context) {
    c.JSON(http.StatusOK, gin.H{"name": "Test02"})
}

func (*Demo) Test03WithGet(c *gin.Context) {
    menu := Database.GetMenu().One("1")
    c.JSON(http.StatusOK, menu)
}

func (*Demo) MigrateWithGet(c *gin.Context) {
    databaseUser := Database.GetUser()
    modelUser := databaseUser.GetModel()
    databaseUser.DB.AutoMigrate(modelUser)
    c.String(http.StatusOK, "ok")
}

func (*Demo) AddWithGet(c *gin.Context) {
    databaseUser := Database.GetUser()
    modelUser := databaseUser.GetModel()
    modelUser.DryUsername = "test"
    databaseUser.DB.Create(modelUser)
    c.String(http.StatusOK, "ok")
}

func (*Demo) OneWithGet(c *gin.Context) {
    databaseUser := Database.GetUser()
    modelUser := databaseUser.GetModel()
    databaseUser.DB.First(modelUser)
    c.JSON(http.StatusOK, modelUser)
}

func (*Demo) OnePlusWithGet(c *gin.Context) {
    databaseUser := Database.GetUser()
    modelUser := databaseUser.GetModel()
    databaseUser.DB.First(modelUser)
    c.JSON(http.StatusOK, modelUser)
}

func (*Demo) ListPlusWithGet(c *gin.Context) {
    databaseUser := Database.GetUser()
    list := make([]Model.User, 0)
    databaseUser.GetDB().Find(&list)
    c.JSON(http.StatusOK, list)
}
