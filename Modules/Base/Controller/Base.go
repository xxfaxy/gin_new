package BaseController

import (
    "github.com/gin-gonic/gin"
)

type Base struct {
}

func (*Base) SendSuccess(c *gin.Context, count int, data interface{}) {
    c.JSON(200, gin.H{
        "code":  "0",
        "msg":   "",
        "count": count,
        "data":  data,
    })
}
