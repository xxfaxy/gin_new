package Struct

import (
    "fmt"
)

type MysqlConfig struct {
    Host     string
    Port     int64
    Username string
    Password string
    Database string
    Charset  string
    Loc      string
}

func (that *MysqlConfig) SetHost(host string) *MysqlConfig {
    that.Host = host
    return that
}

func (that *MysqlConfig) SetPort(port int64) *MysqlConfig {
    that.Port = port
    return that
}

func (that *MysqlConfig) SetUsername(username string) *MysqlConfig {
    that.Username = username
    return that
}

func (that *MysqlConfig) SetPassword(password string) *MysqlConfig {
    that.Password = password
    return that
}

func (that *MysqlConfig) SetDatabase(database string) *MysqlConfig {
    that.Database = database
    return that
}

func (that *MysqlConfig) SetCharset(charset string) *MysqlConfig {
    that.Charset = charset
    return that
}

func (that *MysqlConfig) SetLoc(loc string) *MysqlConfig {
    that.Loc = loc
    return that
}

func (that *MysqlConfig) Get() string {
    return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=%s&parseTime=True&loc=%s", that.Username, that.Password, that.Host, that.Port, that.Database, that.Charset, that.Loc)
}
