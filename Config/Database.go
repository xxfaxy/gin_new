package Config

import (
    "Dry/Constant"
)

func GetDatabase(env string) (m map[string]string) {
    m = make(map[string]string)
    if env == Constant.ENV_LOCAL {
        m = GetDatabaseLocal()
    } else if env == Constant.ENV_PRODUCT {
        m = GetDatabaseProduct()
    }
    return
}
