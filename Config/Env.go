package Config

import (
    "Dry/Constant"
    "strings"
)

func GetEnvList() (list []string) {
    list = []string{Constant.ENV_LOCAL, Constant.ENV_PRODUCT}
    return
}

func GetEnvString(sep string) (result string) {
    result = strings.Join(GetEnvList(), sep)
    return
}
