package Model

import "Dry/Constant"

type User struct {
    Base
    DryUsername string
}

func GetUser() *User {
    that := new(User)
    that.SetConfig(Constant.DEFAULT_VALUE)
    that.SetTable("dry_user")
    return that
}
