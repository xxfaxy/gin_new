package Model

import "Dry/Constant"

type Menu struct {
    Base
}

func GetMenu() *Menu {
    that := new(Menu)
    that.SetConfig(Constant.DEFAULT_VALUE)
    that.SetTable("dry_menu")
    return that
}
