package Function

import (
    "Dry/Constant"
    "time"
)

func GetDateTime() string {
    return time.Now().Format(Constant.DATE_TIME_FORMAT_STD)
}
