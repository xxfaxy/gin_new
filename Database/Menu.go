package Database

import (
    "Dry/Model"
)

type Menu struct {
    Base
}

func GetMenu() *Menu {
    that := new(Menu)
    model := that.GetModel()
    that.Set(model.GetConfig(), model.GetTable())
    return that
}

func (that *Menu) GetModel() *Model.Menu {
    return Model.GetMenu()
}
