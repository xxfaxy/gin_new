package Database

func (that *Base) Fetch(sql string, placeholder []string) (row map[string]string) {
    stmt, _ := that.DB.DB().Prepare(sql)
    defer stmt.Close()
    rows, _ := stmt.Query(that.String2interface(placeholder)...)
    defer rows.Close()
    list := that.Rows2SliceMap(rows)
    if len(list) > 0 {
        row = list[0]
    }
    return
}

func (that *Base) FetchAll(sql string, placeholder []string) (list []map[string]string) {
    stmt, _ := that.DB.DB().Prepare(sql)
    defer stmt.Close()
    rows, _ := stmt.Query(that.String2interface(placeholder)...)
    defer rows.Close()
    list = that.Rows2SliceMap(rows)
    return
}

func (that *Base) InsertUpdateDelete(sqlType, sql string, placeholder []string) (data int64) {
    stmt, _ := that.DB.DB().Prepare(sql)
    defer stmt.Close()
    result, _ := stmt.Exec(that.String2interface(placeholder)...)
    if sqlType == "insert" {
        data, _ = result.LastInsertId()
    } else if sqlType == "update" {
        data, _ = result.RowsAffected()
    } else if sqlType == "delete" {
        data, _ = result.RowsAffected()
    }
    return
}

func (that *Base) Insert(sql string, placeholder []string) int64 {
    return that.InsertUpdateDelete("insert", sql, placeholder)
}

func (that *Base) Update(sql string, placeholder []string) int64 {
    return that.InsertUpdateDelete("update", sql, placeholder)
}

func (that *Base) Delete(sql string, placeholder []string) int64 {
    return that.InsertUpdateDelete("delete", sql, placeholder)
}
