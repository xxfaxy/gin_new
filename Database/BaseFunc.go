package Database

import (
    "database/sql"
)

func (*Base) String2interface(from []string) []interface{} {
    result := make([]interface{}, len(from))
    for k, v := range from {
        result[k] = v
    }
    return result
}

//把字段的值都当成字符串
func (*Base) Rows2SliceMap(rows *sql.Rows) (list []map[string]string) {
    //字段名称
    columns, _ := rows.Columns()
    //多少个字段
    length := len(columns)
    //每一行字段的值
    values := make([]sql.RawBytes, length)
    //保存的是values的内存地址
    pointer := make([]interface{}, length)
    //
    for i := 0; i < length; i++ {
        pointer[i] = &values[i]
    }
    //
    for rows.Next() {
        //把参数展开，把每一行的值存到指定的内存地址去，循环覆盖，values也就跟着被赋值了
        _ = rows.Scan(pointer...)
        //每一行
        row := make(map[string]string)
        for i := 0; i < length; i++ {
            row[columns[i]] = string(values[i])
        }
        list = append(list, row)
    }
    //
    return
}
