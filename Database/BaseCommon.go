package Database

import "Dry/Core"

func (that *Base) Add(kv map[string]string) int64 {
    sqlMaker := Core.GetSqlMaker()
    sqlMaker.SetTable(that.table)
    for k, v := range kv {
        sqlMaker.Set(k, v)
    }
    sql, placeholder := sqlMaker.GetInsertSql()
    return that.Insert(sql, placeholder)
}

func (that *Base) GetAll() (list []map[string]string) {
    sqlMaker := Core.GetSqlMaker()
    sqlMaker.SetField("*")
    sqlMaker.SetTable(that.table)
    sql, placeholder := sqlMaker.GetSelectSql()
    list = that.FetchAll(sql, placeholder)
    return
}

func (that *Base) One(id string) (row map[string]string) {
    sqlMaker := Core.GetSqlMaker()
    sqlMaker.SetField("*")
    sqlMaker.SetTable(that.table)
    sqlMaker.Where("id", "=", id)
    sql, placeholder := sqlMaker.GetSelectSql()
    row = that.Fetch(sql, placeholder)
    return
}
