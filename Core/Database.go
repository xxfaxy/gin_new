package Core

import (
    "github.com/jinzhu/gorm"
    _ "github.com/jinzhu/gorm/dialects/mysql"
    "sync"
)

type Database struct {
    m sync.Map
}

func GetDatabase(config, dns string) (db *gorm.DB, err error) {
    that := new(Database)
    value, ok := that.m.Load(config)
    if ok {
        db = value.(*gorm.DB)
        return
    }
    db, err = gorm.Open("mysql", dns)
    if err != nil {
        return
    }
    that.m.Store(config, db)
    return
}
