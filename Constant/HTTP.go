package Constant

const (
    HTTP_REQUEST_GET     = "GET"
    HTTP_REQUEST_POST    = "POST"
    DEFAULT_VALUE        = "default"
    ENV_LOCAL            = "Local"
    ENV_PRODUCT          = "Product"
    DATE_TIME_FORMAT_STD = "2006-01-02 15:04:05"
)
